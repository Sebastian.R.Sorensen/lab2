package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{
    
    ArrayList<FridgeItem> myFridge = new ArrayList<>();

    int max_size = 20;

    @Override
    public int totalSize() {
        return max_size;
    }

    @Override
    public int nItemsInFridge() {
        // TODO Auto-generated method stub

        return myFridge.size();
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        // TODO Auto-generated method stub
        if(myFridge.size()<max_size){
            myFridge.add(item);
            return true;
        }
        else {
            return false;
        }      
    }

    @Override
    public void takeOut(FridgeItem item) {
        // TODO Auto-generated method stub
        if(myFridge.contains(item)){
            myFridge.remove(item);
        }
        else{
            throw new NoSuchElementException();
        }
    }

    @Override
    public void emptyFridge() {
        // TODO Auto-generated method stub
        myFridge.clear();
        
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        // TODO Auto-generated method stub
        ArrayList<FridgeItem> removedExpiredFood = new ArrayList<>();
        for(FridgeItem i: myFridge){
            if(i.hasExpired()){
                removedExpiredFood.add(i);
            }
        }
        myFridge.removeAll(removedExpiredFood);

        return removedExpiredFood;
    }
}
